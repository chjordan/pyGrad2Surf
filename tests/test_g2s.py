#!/usr/bin/env python

import numpy as np

from pyGrad2Surf.g2s import g2s


def test_simple_gradient():
    correct = [[-28.5, -28.,  -26.5, -24.,  -20.5, -16.,  -10.5,  -4.,    3.5,  12. ],
               [-28.,  -27.5, -26.,  -23.5, -20.,  -15.5, -10.,   -3.5,   4.,   12.5],
               [-26.5, -26.,  -24.5, -22.,  -18.5, -14.,   -8.5,  -2.,    5.5,  14. ],
               [-24.,  -23.5, -22.,  -19.5, -16.,  -11.5,  -6.,    0.5,   8.,   16.5],
               [-20.5, -20.,  -18.5, -16.,  -12.5,  -8.,   -2.5,   4.,   11.5,  20. ],
               [-16.,  -15.5, -14.,  -11.5,  -8.,   -3.5,   2.,    8.5,  16.,   24.5],
               [-10.5, -10.,   -8.5,  -6.,   -2.5,   2.,    7.5,  14.,   21.5,  30. ],
               [ -4.,   -3.5,  -2.,    0.5,   4.,    8.5,  14.,   20.5,  28.,   36.5],
               [  3.5,   4.,    5.5,   8.,   11.5,  16.,   21.5,  28.,   35.5,  44. ],
               [ 12.,   12.5,  14.,   16.5,  20.,   24.5,  30.,   36.5,  44.,   52.5]]

    dx, dy = np.meshgrid(np.arange(10, dtype=float),
                         np.arange(10, dtype=float))
    reconstructed = g2s(dx[0, :], dy[:, 0], dx, dy)

    assert np.all(np.isclose(correct, reconstructed, atol=1e-5))


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
