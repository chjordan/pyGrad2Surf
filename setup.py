from setuptools import setup
from codecs import open
from os import path
import re


here = path.abspath(path.dirname(__file__))
with open(path.join(here, "pyGrad2Surf/__init__.py")) as f:
    contents = f.read()
    version_number = re.search(r"__version__ = \"(\S+)\"", contents).group(1)

with open(path.join(here, "README.rst"), encoding="utf-8") as f:
    long_description = f.read()

setup(name="pyGrad2Surf",
      version=version_number,
      description="A python translation of the Grad2Surf software provided by Harker & O'Leary",
      long_description=long_description,
      url="https://github.com/cjordan/pyGrad2Surf",
      author="Christopher Jordan",
      author_email="christopherjordan87@gmail.com",
      license="MIT",
      keywords="signal processing",
      packages=["pyGrad2Surf"],
      install_requires=["numpy",
                        "scipy"],
)
